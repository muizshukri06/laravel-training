<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {   
        $products = Product::all();
        return view('product.index')->with('products', $products);
        // return view('product.index')->compact('product');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('product.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validate = $request->validate([
            'name' => 'required',
            'price' => 'required',
        ]);
        
        // dd($validate);
        try {
            Product::create($validate);

            return redirect(route('product.index'));
        } catch (\Throwable $th) {
            return back()->withErrors('Error saving product!!');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id) // 1
    {
        $product = Product::find($id);

        return view('product.show')->with('product',$product);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $product = Product::find($id);

        return view('product.edit')->with('product',$product);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $validate = $request->validate([
            'name' => 'required',
            'price' => 'required',
        ]);
        
        // dd($validate);
        try {
            Product::find($id)->update($validate);

            return redirect(route('product.index'));
        } catch (\Throwable $th) {
            return back()->withErrors('Error updating product!!');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            Product::find($id)->delete();

            return redirect(route('product.index'));
        } catch (\Throwable $th) {
            return back()->withErrors('Error deleting product!!');
        }
    }
}
