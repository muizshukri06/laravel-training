@extends('layout')

@section('header')
    Product Page
@endsection

@section('content')
    <a type="button" class="btn btn-success" href="{{ route('product.create') }}">Add Product</a>

    <table class="table">
        <thead>
            <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Price</th>
            <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            @php
                $index = 1
            @endphp

            @forelse ($products as $product)
            <tr>
            <th scope="row">{{ $index++ }}</th>
            <td>{{ $product->name }}</td>
            <td>{{ $product->price }}</td>
            <td>
                <a type="button" class="btn btn-primary" href="{{ route('product.show', $product->id ) }}">View</a>
                <a type="button" class="btn btn-warning" href="{{ route('product.edit', $product->id ) }}">Edit</a>
                <!-- onsubmit="return confirm('Are you sure you want to delete {{ $product->name }}?');" -->
                <form onsubmit="return confirm('Are you sure you want to delete {{ $product->name }}?');" action="{{ route('product.delete', $product->id) }}" method="post">
                    @csrf
                    @method('delete')
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>    
            </td>
            </tr>
            @empty
                <tr>
                    <th colspan="4" class="text-center">No Data...</th>
                </tr>
            @endforelse
        </tbody>
    </table>
    @endsection