@extends('layout')

@section('header')
    {{ $product->name }}
@endsection

@section('content')
        <div class="mb-3">
            <label for="name" class="form-label">Name</label>
            <input type="text" class="form-control" id="name" value="{{ $product->name }}" readonly>
        </div>
        <div class="mb-3">
            <label for="price" class="form-label">Price</label>
            <input type="number" class="form-control" id="price" value="{{ $product->price }}" readonly>
        </div>
@endsection